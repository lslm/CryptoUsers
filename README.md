This project is a simple form created to create a user, access its data,
update its data and delete it (also known as "CRUD"). I am working in this C# .NET application 
to learn how to connect my future apps to a PostgreSQL database, using EF.Npgsql