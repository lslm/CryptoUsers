﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CryptoUsers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DAL acesso;

        public MainWindow()
        {
            int id;
            acesso = new DAL();
            InitializeComponent();

            txtNome.Text = "";
            txtEmail.Text = "";
            txtId.Text = "";
            txtIdade.Text = "";
        }

        private void UpdateGrid()
        {
            dgUsers.ItemsSource = acesso.GetRegisters().DefaultView;
        }

        private void btnExibir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UpdateGrid();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex);
            }
        }

        private void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                acesso.InsertRegister(txtNome.Text, txtEmail.Text, int.Parse(txtIdade.Text));
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex);
            }
            finally
            {
                UpdateGrid();
            }
        }

        private void btnAtualizar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                acesso.UpdateRegister(int.Parse(txtId.Text), txtNome.Text, txtEmail.Text, int.Parse(txtIdade.Text));
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex);
            }
            finally
            {
                UpdateGrid();
            }
        }

        private void btnDeletar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                acesso.DeleteRegister(int.Parse(txtId.Text));
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex);
            }
            finally
            {
                UpdateGrid();
            }
        }
    }
}
