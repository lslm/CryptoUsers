﻿using System;
using System.Data;
using Npgsql;
using System.Text;
using System.Collections;

/*
 * Classe responsável por permitir
 * comunicação com o banco de dados
 * PostgreSQL e realizar as operações
 * CREATE, RETRIEVE, UPDATE e DELETE
*/

namespace CryptoUsers
{
    class DAL
    {
        static string serverName = "localhost";
        static string port = "5432";
        static string userName = "postgres";
        static string password = "ubowpxnx";
        static string databaseName = "cadastro";
        string connString = null;
        NpgsqlConnection pgsqlConnection;
        public DAL()
        {
            // conversão da string de conexão para UTF-8
            connString = String.Format("Server={0};Port={1};User Id={2};Password={3};Database={4};", serverName, port, userName, password, databaseName);
            byte[] utf = Encoding.UTF8.GetBytes(connString);
            connString = Encoding.UTF8.GetString(utf);
        }

        private void OpenConnection()
        {
            try
            {
                pgsqlConnection = new NpgsqlConnection(connString);
                pgsqlConnection.Open();
            }
            catch(NpgsqlException ex)
            {
                throw ex;
            }
        }

        private void CloseConnetion()
        {
            pgsqlConnection.Close();
        }

        public DataTable GetRegisters()
        {
            // Instância da classe DataTable
            // para receber os registro do BD
            DataTable dt = new DataTable("Users");

            try
            {
                // abre a conexão com o Postgres e define a instrução SQL
                OpenConnection();
                
                // query a ser executada
                string cmdSeleciona = "SELECT * FROM users ORDER BY id";

                // execução da query
                NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection);

                // Objeto Adpt preenche o objeto dt
                Adpt.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnetion();
            }

            return dt;
        }

        public DataTable GetRegisterById(int id)
        {
            DataTable dt = new DataTable();

            try
            {
                OpenConnection();

                string cmdSeleciona = "SELECT * FROM users WHERE id = " + id;

                using (NpgsqlDataAdapter Adpt = new NpgsqlDataAdapter(cmdSeleciona, pgsqlConnection))
                {
                    Adpt.Fill(dt);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnetion();
            }

            return dt;
        }

        public void InsertRegister(string nome, string email, int idade)
        {
            try
            {
                OpenConnection();

                string cmdInsert = String.Format("INSERT INTO users (id, nome, email, idade) VALUES(nextval('seq_id'), '{0}', '{1}', {2})", nome, email, idade);

                using (NpgsqlCommand pgsqlCommand = new NpgsqlCommand(cmdInsert, pgsqlConnection))
                {
                    pgsqlCommand.ExecuteNonQuery();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnetion();
            }
        }

        public void UpdateRegister(int id, string nome, string email, int idade)
        {
            try
            {
                OpenConnection();

                string cmdUpdate = String.Format("UPDATE users SET nome = '" + nome + "', email = '" + email + "', idade = " + idade + "WHERE id = " + id);

                using (NpgsqlCommand pgsqlCommand = new NpgsqlCommand(cmdUpdate, pgsqlConnection))
                {
                    pgsqlCommand.ExecuteNonQuery();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnetion();
            }
        }

        public void DeleteRegister(int id)
        {
            try
            {
                OpenConnection();

                string cmdDelete = String.Format("DELETE FROM users WHERE id = " + id);

                using (NpgsqlCommand pgsqlCommand = new NpgsqlCommand(cmdDelete, pgsqlConnection))
                {
                    pgsqlCommand.ExecuteNonQuery();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnetion();
            }
        }
    }
}
